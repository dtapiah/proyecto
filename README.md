# Proyecto Laravel v5.4!

### Instrucciones para descargar el social login

- Clonar el repositorio:
	- git clone https://dtapiah@bitbucket.org/dtapiah/proyecto_ingweb.git


- Crear una base de datos en MySQL que se llame **proyecto**

- En la consola, navegar hasta la carpeta con el proyecto

- Migrar las tablas a la base de datos con el comando:
	- *php artisan migrate*


- Arrancar el servidor con el comando:
	- *php artisan serve*


- En el navegador se ingresará a la aplicación desde:
	- *localhost:8000*

Ojo: La carpeta debe estar en **htdocs** y debe llamarse **blog**.